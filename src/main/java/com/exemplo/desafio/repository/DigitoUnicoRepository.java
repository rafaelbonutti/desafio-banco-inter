package com.exemplo.desafio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exemplo.desafio.model.DigitoUnico;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {

	List<DigitoUnico> findByUsuarios_id(Long usuario_id);

	DigitoUnico findOneByNumeroAndMultiplicador(String numero, Integer multiplicador);
}
