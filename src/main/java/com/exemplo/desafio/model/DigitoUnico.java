package com.exemplo.desafio.model;

import java.util.Objects;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DigitoUnico {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String numero;
	private Integer multiplicador;
	private Integer resultado;

	@JsonIgnore
	@ManyToMany(mappedBy = "digitosUnicosCalculados")
	private Set<Usuario> usuarios;

	public Set<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public DigitoUnico() {
	}

	public DigitoUnico(Long id, String n, int k, int p) {
		this.id = id;
		this.numero = n;
		this.multiplicador = k;
		this.resultado = p;
	}

	public DigitoUnico(String n, int k) {
		this.numero = n;
		this.multiplicador = k;
	}

	@Override
	public int hashCode() {
		return Objects.hash(multiplicador, numero);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DigitoUnico other = (DigitoUnico) obj;
		return Objects.equals(multiplicador, other.multiplicador) && Objects.equals(numero, other.numero);
	}

	@Override
	public String toString() {
		return "DigitoUnico [id=" + id + ", numero=" + numero + ", multiplicador=" + multiplicador + ", resultado="
				+ resultado + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getMultiplicador() {
		return multiplicador;
	}

	public void setMultiplicador(Integer multiplicador) {
		this.multiplicador = multiplicador;
	}

	public Integer getResultado() {
		return resultado;
	}

	public void setResultado(Integer resultado) {
		this.resultado = resultado;
	}
}