package com.exemplo.desafio.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exemplo.desafio.model.DigitoUnico;
import com.exemplo.desafio.service.DigitoUnicoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({ "/digitosunicos" })
@Api(value = "Dígitos Únicos")
public class DigitoUnicoController {

	private DigitoUnicoService service;

	DigitoUnicoController(DigitoUnicoService digitoUnicoService) {
		this.service = digitoUnicoService;
	}

	@GetMapping
	@ApiOperation(value = "Recupera todos os Dígitos Únicos já calculados")
	public List<DigitoUnico> findAll(@RequestParam("usuario_id") Optional<Long> usuario_id) {
		return service.findAll(usuario_id);
	}

	@GetMapping(path = { "/{id}" })
	@ApiOperation(value = "Recupera um Dígito Único")
	public ResponseEntity<DigitoUnico> findById(@PathVariable long id) {
		return service.findById(id);
	}

	@PostMapping
	@ApiOperation(value = "Calcula um Dígito Único")
	public DigitoUnico calcularDigitoUnico(@RequestParam("numero") String numero,
			@RequestParam("multiplicador") Integer multiplicador,
			@RequestParam("usuario_id") Optional<Long> usuario_id) {
		return service.calcularDigitoUnico(numero, multiplicador, usuario_id);
	}

}