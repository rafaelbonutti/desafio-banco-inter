package com.exemplo.desafio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exemplo.desafio.model.Usuario;
import com.exemplo.desafio.service.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping({ "/usuarios" })
@Api(value = "Usuários")
public class UsuarioController {

	private UsuarioService service;

	@Autowired
	UsuarioController(UsuarioService service) {
		this.service = service;
	}

	@GetMapping
	@ApiOperation(value = "Recupera todos os Usuários cadastrados")
	public List<Usuario> findAll() {
		return service.findAll();
	}

	@GetMapping(path = { "/{id}" })
	@ApiOperation(value = "Recupera um Usuário")
	public ResponseEntity<Usuario> findById(@PathVariable long id) {
		return service.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	@ApiOperation(value = "Cria um Usuário")
	public Usuario create(@RequestBody Usuario usuario) {
		return service.save(usuario);
	}

	@PutMapping(value = "/{id}")
	@ApiOperation(value = "Atualiza um Usuário")
	public ResponseEntity<Usuario> update(@PathVariable("id") long id, @RequestBody Usuario usuario) {
		return service.findById(id).map(record -> {
			record.setNome(usuario.getNome());
			record.setEmail(usuario.getEmail());
			record.setChavePublica(usuario.getChavePublica());
			Usuario updated = service.save(record);
			return ResponseEntity.ok().body(updated);
		}).orElse(ResponseEntity.notFound().build());
	}

	@DeleteMapping(path = { "/{id}" })
	@ApiOperation(value = "Exclui um Usuário")
	public ResponseEntity<?> delete(@PathVariable("id") long id) {
		return service.findById(id).map(record -> {
			service.deleteById(id);
			return ResponseEntity.ok().build();
		}).orElse(ResponseEntity.notFound().build());
	}
}