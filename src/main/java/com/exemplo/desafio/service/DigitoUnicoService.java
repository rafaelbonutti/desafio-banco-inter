package com.exemplo.desafio.service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.exemplo.desafio.model.DigitoUnico;
import com.exemplo.desafio.model.Usuario;
import com.exemplo.desafio.repository.DigitoUnicoRepository;
import com.exemplo.desafio.repository.UsuarioRepository;

@Service
public class DigitoUnicoService {

	@Autowired
	private DigitoUnicoRepository digitoUnicoRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	public int calcular(String numero, Integer multiplicador) {
		Integer soma = Arrays.stream(numero.split("")).filter(s -> s.matches("\\d+")).mapToInt(Integer::valueOf).sum();
		String resultado = String.valueOf(multiplicador * soma);
		return resultado.length() == 1 ? Integer.valueOf(resultado) : calcular(resultado, 1);
	}

	public List<DigitoUnico> findAll(Optional<Long> usuario_id) {

		if (usuario_id.isPresent()) {
			Optional<Usuario> u = usuarioRepository.findById(usuario_id.get());
			if (u.isPresent()) {
				return digitoUnicoRepository.findByUsuarios_id(usuario_id.get());
			}
		}
		return digitoUnicoRepository.findAll();
	}

	public ResponseEntity<DigitoUnico> findById(long id) {
		return digitoUnicoRepository.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

	public DigitoUnico save(DigitoUnico du) {
		return digitoUnicoRepository.save(du);
	}

	public DigitoUnico calcularDigitoUnico(String numero, Integer multiplicador, Optional<Long> usuario_id) {

		DigitoUnico du = null;
		DigitoUnico cached = (DigitoUnico) CacheService.getInstance().get(new DigitoUnico(numero, multiplicador));

		if (cached == null) {
			du = digitoUnicoRepository.findOneByNumeroAndMultiplicador(numero, multiplicador);
			if (du == null) {
				int resultado = calcular(numero, multiplicador);
				du = new DigitoUnico(null, numero, multiplicador, resultado);
				du = digitoUnicoRepository.saveAndFlush(du);
			}
			CacheService.getInstance().put(du);
		} else {
			du = cached;
		}

		if (usuario_id.isPresent()) {
			Optional<Usuario> u = usuarioRepository.findById(usuario_id.get());
			if (u.isPresent()) {
				u.get().getDigitosUnicosCalculados().add(du);
				usuarioRepository.saveAndFlush(u.get());
			}
		}

		return du;
	}
}
