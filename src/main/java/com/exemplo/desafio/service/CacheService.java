package com.exemplo.desafio.service;

import java.util.LinkedHashMap;
import java.util.Map;

public class CacheService {

	private static final int TAMANHO_MAXIMO = 10;
	private static CacheService INSTANCE = new CacheService();

	private CacheService() {
	}

	public static CacheService getInstance() {
		return INSTANCE;
	}

	private Map<Object, Object> cacheObjeto = new LinkedHashMap<>(TAMANHO_MAXIMO);

	public synchronized void put(Object objeto) {
		if (cacheObjeto.size() == TAMANHO_MAXIMO) {
			cacheObjeto.remove(cacheObjeto.entrySet().iterator().next().getKey());
		}
		cacheObjeto.put(objeto, objeto);
		// cacheObjeto.entrySet().forEach(System.out::println)
	}

	public synchronized Object get(Object object) {
		synchronized (cacheObjeto) {
			return cacheObjeto.get(object);
		}
	}
}
