package com.exemplo.desafio.service;

import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.exemplo.desafio.model.Usuario;
import com.exemplo.desafio.repository.UsuarioRepository;

@Service
public class UsuarioService {

	private static final String ALGORITMO = "RSA";

	@Autowired
	private UsuarioRepository usuarioRepository;

	public List<Usuario> findAll() {
		return usuarioRepository.findAll();
	}

	public Optional<Usuario> findById(long id) {
		return usuarioRepository.findById(id);
	}

	public Usuario save(Usuario usuario) {

		if (!StringUtils.isEmpty(usuario.getChavePublica())) {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(usuario.getChavePublica()));
			KeyFactory keyFactory;
			PublicKey publicKey = null;
			try {
				keyFactory = KeyFactory.getInstance(ALGORITMO);
				publicKey = keyFactory.generatePublic(keySpec);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				e.printStackTrace();
			}

			usuario.setEmail(criptografa(usuario.getEmail(), publicKey));
			usuario.setNome(criptografa(usuario.getNome(), publicKey));
		}
		return usuarioRepository.save(usuario);
	}

	public void deleteById(long id) {
		usuarioRepository.deleteById(id);
	}

	public static String criptografa(String texto, Key chavePublica) {
		byte[] cipherText = null;

		try {
			final Cipher cipher = Cipher.getInstance(ALGORITMO);
			cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
			cipherText = cipher.doFinal(texto.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Base64.getEncoder().encodeToString(cipherText);
	}

	public static String decriptografa(byte[] texto, Key chavePrivada) {
		byte[] dectyptedText = null;

		try {
			final Cipher cipher = Cipher.getInstance(ALGORITMO);
			cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
			dectyptedText = cipher.doFinal(texto);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return Base64.getEncoder().encodeToString(dectyptedText);
	}

}
