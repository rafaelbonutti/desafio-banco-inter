package com.exemplo.desafio.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.exemplo.desafio.service.DigitoUnicoService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DigitoUnicoServiceTest {

	@Autowired
	private DigitoUnicoService service;

	@Test
	public void calcularDigitoUnico() {

		int resultado = service.calcular("9875", 4);
		assertThat(resultado).isEqualTo(8);

		resultado = service.calcular("123", 4);
		assertThat(resultado).isEqualTo(6);

		resultado = service.calcular(String.valueOf(new Random().nextInt()), 9);
		assertThat(resultado).isEqualTo(9);
	}

}
