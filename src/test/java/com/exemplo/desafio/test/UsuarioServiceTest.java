package com.exemplo.desafio.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.exemplo.desafio.model.Usuario;
import com.exemplo.desafio.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest {

	private static final int TAMANHO_CHAVE = 2048;
	private static final String ALGORITMO = "RSA";

	@Autowired
	private UsuarioService service;

	@Test
	public void testeSalvarUsuario() throws Exception {

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITMO);
		keyPairGenerator.initialize(TAMANHO_CHAVE);
		KeyPair keyPair = keyPairGenerator.generateKeyPair();
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITMO);
		X509EncodedKeySpec keySpec = keyFactory.getKeySpec(keyPair.getPublic(), X509EncodedKeySpec.class);

		// MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5486DqM1byk6z70lR9zQ+0fi6WeOUDcFOJUYBp2RS28qU6ItOnC1fzcy4Gh2svQzgcvw7v3mpBoUl4xp/c5ZrvuISiBj2xgxSUM1XRoASb7LFGeKxD247bkb4TYO5rwLz08MIRr4Hq9Vm6Me/d0DHUIhev0sEJqbdcu5lru4XiIlrosufV3YnKW/jKX4rf0nt2MZxieoqnd7Z6OJZSvWA8eLOTaIouOUv8/qbKWHnKoP/B3XbURl+htZzfgwFBvkkoauUdHNTr1d14mLUMMNBgQLJ7/yNClkZx2q3Bdl4u78W6jAiR2Nw2IV9+yVtJMhvOTGtxPpaNtjmjNORV1VOQIDAQAB

		Usuario usuario = new Usuario(null, "Teste", "teste@teste.com",
				Base64.getEncoder().encodeToString(keySpec.getEncoded()));
		usuario = service.save(usuario);

		assertThat(usuario.getId()).isNotNull();
	}

}
