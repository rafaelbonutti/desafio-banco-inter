# Desafio Banco Inter


## Installing

```
git clone https://rafaelbonutti@bitbucket.org/rafaelbonutti/desafio-banco-inter.git
```

```
mvn install
```

```
java -jar target/desafio-0.0.1-SNAPSHOT.jar
```

## Running the tests

```
mvn test
```

## API's

* [Documentação] (http://localhost:8080/api/swagger-ui.html)
* [Dígito Único] (http://localhost:8080/api/digitosunicos)
* [Usuario] (http://localhost:8080/api/usuarios)

## Built With

* [Spring Boot](http://www.spring.io)
* [Maven](https://maven.apache.org/)

## TODO

* Melhorar a cobertaura de testes unitários
* Melhorar a documentação da API
* Implementar validações nas camadas Service/Model
* Implementar tratamento de exceções